package com.tool.shiro.mapper;

import com.tool.shiro.pojo.ShiroUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ShiroUserMapper {

	// 添加shiro_user用户
	int insertUser(ShiroUser shiroUser);

	// 查询所有用户信息
	List<ShiroUser> queryAllUserInfo();

	// 查询指定用户信息-通过account
	ShiroUser queryUserByAccount(@Param("account") String account);

	// 修改指定用户
	int updateShiroUser(ShiroUser shiroUser);

	// 删除指定ID shiro_user 表用户
	int deleteShiroUser(ShiroUser shiroUser);

}
