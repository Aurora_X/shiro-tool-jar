package com.tool.shiro.mapper.basic;

import com.tool.shiro.pojo.ShiroAdminManage;
import com.tool.shiro.pojo.ShiroResource;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
//@Repository
public interface ShiroAdminMapper {

	// 获取所有资源权限
	List<ShiroResource> selectAllShiroResources();

	// 修改权限，返回更新数量
	int updateShiroResource(ShiroResource shiroResource);

	// 查询表是否存在
	int existTables(String database, List<String> tableNames);

	// 删除 指定ID shiro管理员
	int deleteShiroAdminById(ShiroAdminManage shiroAdminManage);

	// 更新shiro管理员
	int updateShiroAdmin(ShiroAdminManage shiroAdminManage);
}
