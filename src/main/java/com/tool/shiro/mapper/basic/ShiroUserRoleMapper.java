package com.tool.shiro.mapper.basic;

import com.tool.shiro.pojo.ShiroUserRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ShiroUserRoleMapper {

	// 查询 指定用户ID 所有角色ID
	List<Integer> queryUserRolesId(@Param("id") Integer userId, @Param("tableName") String tableName);

	// 添加用户角色
	int insertUserRole(ShiroUserRole shiroUserRole);

	// 删除 指定id 用户角色
	int deleteUserRoleById(ShiroUserRole shiroUserRole);

	// 删除 指定用户ID 所有角色
	int deleteUserAllRole(@Param("userId") Integer userId);

	// 更新 指定id 用户角色
	int updateUserRole(ShiroUserRole shiroUserRole);

}
