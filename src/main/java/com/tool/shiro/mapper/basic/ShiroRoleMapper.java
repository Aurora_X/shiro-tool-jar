package com.tool.shiro.mapper.basic;

import com.tool.shiro.pojo.ShiroRole;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ShiroRoleMapper {

	// 查询所有角色
	List<ShiroRole> queryAllRoles();

	// 查询指定类型角色
	List<ShiroRole> queryAllRolesByUserType(String userType);

	// 查询指定用户id所有角色
	List<ShiroRole> queryRoleByRolesId(List<Integer> idList);

	// 添加角色
	int insertRole(ShiroRole shiroRole);

	// 删除角色-通过id
	int deleteRole(ShiroRole shiroRole);

	// 更新角色
	int updateRole(ShiroRole shiroRole);

}
