package com.tool.shiro.mapper.basic;

import com.tool.shiro.pojo.ShiroRole;
import com.tool.shiro.pojo.ShiroRolePermission;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ShiroRolePermissionMapper {

	// 查询 指定角色ID 所有权限ID
	List<Integer> queryRolePermissionsId(List<Integer> idList);

	// 查询 指定角色ID 所有权限ID
	List<Integer> queryRolePermissionsByRoleId(ShiroRole shiroRole);

	// 添加角色权限
	int insertRolePermission(ShiroRolePermission shiroRolePermission);

	// 删除 指定id 角色权限
	int deleteRolePermissionById(ShiroRolePermission shiroRolePermission);

	// 删除 指定角色ID 所有权限
	int deleteRoleAllPermission(@Param("roleId") Integer roleId);

	// 更新 指定id 角色权限
	int updateRolePermission(ShiroRolePermission shiroRolePermission);

}
