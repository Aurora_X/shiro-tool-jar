package com.tool.shiro.mapper.basic;

import com.tool.shiro.pojo.ShiroPermission;
import com.tool.shiro.pojo.ShiroRole;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ShiroPermissionMapper {

	// 查询所有权限
	List<ShiroPermission> queryAllPermissions();

	// 查询 指定ID 权限
	List<ShiroPermission> queryPermissionsByIds(List<Integer> idList);

	// 添加权限
	int insertPermission(ShiroPermission shiroPermission);

	// 删除 指定ID 权限
	int deletePermission(ShiroPermission shiroPermission);

	// 更新 指定ID 权限
	int updatePermission(ShiroPermission shiroPermission);

}
