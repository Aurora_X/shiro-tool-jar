package com.tool.shiro.mapper.basic;

import com.tool.shiro.pojo.ShiroResource;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ShiroResourceMapper {

	// 添加资源
	int insertResource(ShiroResource shiroResource);

	// 删除资源-通过id
	int deleteResource(ShiroResource shiroResource);

}
