package com.tool.shiro.util;

import java.util.Random;

public class SaltUtils {

    /**
     * 生成指定位数的salt盐
     * @param n     位数
     * @return      n位数盐
     */
    public static String getSalt(int n){
        char[] chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ~!@#$%^&*()-=_+{}[];':,./<>?".toCharArray();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n; i++) {
            char aChar = chars[new Random().nextInt(chars.length)];
            sb.append(aChar);
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(  getSalt(16)  );
    }
}
