package com.tool.shiro.util;

import com.tool.shiro.annotation.ApiInfoAnnotation;
import com.tool.shiro.pojo.ShiroPermission;
import com.tool.shiro.pojo.ShiroResource;
import org.reflections.Reflections;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class AnnoManageUtil {

    /**
     * 获取指定文件下面的RequestMapping方法保存在mapp中
     *
     * @param packageName
     * @return
     */
    public static Map<String, ShiroPermission> getRequestMappingMethod(String packageName) {
        Reflections reflections = new Reflections(packageName);
        Set<Class<?>> classesList = reflections.getTypesAnnotatedWith(ApiInfoAnnotation.class);

        // 存放url和ApiInfoAnnotation的对应关系
        Map<String, ShiroPermission> mapp = new HashMap<String, ShiroPermission>();
        for (Class classes : classesList) {
            //得到该类下面的所有方法
            Method[] methods = classes.getDeclaredMethods();

            for (Method method : methods) {
                //得到该类下面的RequestMapping注解
                ApiInfoAnnotation annotation = method.getAnnotation(ApiInfoAnnotation.class);
                if (null != annotation) {
                    ShiroResource shiroResource = new ShiroResource();
                    try {
                        shiroResource.setDescription((String) classes.newInstance());
                    } catch (InstantiationException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                    shiroResource.setMethod(method.getName());
                }
            }
        }
        return mapp;
    }
}
