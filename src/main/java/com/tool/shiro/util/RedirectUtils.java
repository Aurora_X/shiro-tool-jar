package com.tool.shiro.util;

import org.springframework.http.HttpRequest;
import org.springframework.web.servlet.support.RequestContext;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RedirectUtils {

    /**
     * 解决Ajax请求时无法重定向问题
     */
    public static void sendRedirect(HttpServletResponse response, String redirectUrl) throws IOException {
        try {
            // 这里并不是设置跳转页面，而是将重定向的地址发给前端，让前端执行重定向
            // 设置跳转地址
            response.setHeader("redirectUrl", redirectUrl);
            //设置跳转使能
            response.setHeader("enableRedirect","true");
            response.flushBuffer();
        } catch (IOException e) {
            throw new IOException(e);
        }
    }

}
