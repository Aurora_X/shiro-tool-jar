package com.tool.shiro.pojo;

import java.io.Serializable;
import java.util.List;

/**
 * shiro工具角色实体类
 */
public class ShiroRole implements Serializable {

    // 序列化ID
    public static final long serialVersionUID = 1L;

    // 角色id
    private Integer id;
    // 角色名称
    private String name;
    // 角色权限[一个角色可能有多个权限]
    private List<ShiroPermission> shiroPermissions;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public ShiroRole() {
    }

    public ShiroRole(Integer id, String name, List<ShiroPermission> shiroPermissions) {
        this.id = id;
        this.name = name;
        this.shiroPermissions = shiroPermissions;
    }

    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ShiroPermission> getPermissions() {
        return shiroPermissions;
    }

    public void setPermissions(List<ShiroPermission> shiroPermissions) {
        this.shiroPermissions = shiroPermissions;
    }

    @Override
    public String toString() {
        return "ShiroRole{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", shiroPermissions=" + shiroPermissions +
                '}';
    }
}
