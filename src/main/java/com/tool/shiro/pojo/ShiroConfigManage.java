package com.tool.shiro.pojo;

import java.io.Serializable;

/**
 * shiro工具管理员配置信息实体类
 */
public class ShiroConfigManage implements Serializable {

    // 序列化ID
    public static final long serialVersionUID = 1L;

    // 配置id
    private Integer id;
    // 配置名称
    private String name;
    // 配置参数
    private String value;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public ShiroConfigManage() {
    }

    public ShiroConfigManage(Integer id, String name, String value) {
        this.id = id;
        this.name = name;
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ShiroConfigManage{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
