package com.tool.shiro.pojo;

import java.io.Serializable;

/**
 * 接口信息实体类
 */
public class ApiInfo implements Serializable {

    // 序列化ID
    public static final long serialVersionUID = 1L;

    // 接口名称
    private String name;
    // 接口URL
    private String url;
    // 接口记号 [接口访问角色Role/权限Permission/宾客guest等]
    private String mark;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public ApiInfo() {
    }

    public ApiInfo(String name, String url, String mark) {
        this.name = name;
        this.url = url;
        this.mark = mark;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }
}
