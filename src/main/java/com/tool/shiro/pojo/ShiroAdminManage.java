package com.tool.shiro.pojo;

import java.io.Serializable;

/**
 * shiro工具管理员实体类
 */
public class ShiroAdminManage implements Serializable {

    // 序列化ID
    private static final long serialVersionUID = 1L;

    // 管理员id
    private Integer id;
    // 管理员账号
    private String account;
    // 管理员密码
    private String password;
    // 管理员密码盐值
    private String salt;
    // 管理员邮箱
    private String email;
    // 管理员手机号
    private String phone;

    public ShiroAdminManage() {
    }

    public ShiroAdminManage(Integer id, String account, String password, String salt, String email, String phone) {
        this.id = id;
        this.account = account;
        this.password = password;
        this.salt = salt;
        this.email = email;
        this.phone = phone;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "ShiroAdminManage{" +
                "id=" + id +
                ", account='" + account + '\'' +
                ", password='" + password + '\'' +
                ", salt='" + salt + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
