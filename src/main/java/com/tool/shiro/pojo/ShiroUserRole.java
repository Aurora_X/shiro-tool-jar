package com.tool.shiro.pojo;

import java.io.Serializable;

/**
 * shiro工具 用户-角色 实体类
 */
public class ShiroUserRole implements Serializable {

    // 序列化ID
    public static final long serialVersionUID = 1L;

    // 用户-角色表id
    private Integer id;
    // 用户id
    private Integer userId;
    // 角色id
    private Integer roleId;
    // 用户所在表
    private String userType;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public ShiroUserRole() {
    }

    public ShiroUserRole(Integer id, Integer userId, Integer roleId, String userType) {
        this.id = id;
        this.userId = userId;
        this.roleId = roleId;
        this.userType = userType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    @Override
    public String toString() {
        return "ShiroUserRole{" +
                "id=" + id +
                ", userId=" + userId +
                ", roleId=" + roleId +
                ", userType=" + userType +
                '}';
    }
}
