package com.tool.shiro.pojo;

import org.apache.ibatis.type.Alias;

import java.io.Serializable;

/**
 * shiro工具资源实体类
 */

// 设置mybatis类型别名
@Alias(value = "ShiroResource")
public class ShiroResource implements Serializable {

    // 序列化ID
    public static final long serialVersionUID = 1L;

    // 权限id
    private Integer id;
    // 权限类型 [url / resources / ...]
    private String type;
    // 权限值 [url: API接口地址 / resources: 资源信息 / ...]
    private String value;
    // 访问类型 [GET / POST / HEAD / PUT / ...]
    private String method;
    // 描述
    private String description;
    // shiro过滤器Filter [anon / authc / ...]
    private String control;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public ShiroResource() {
    }

    public ShiroResource(Integer id, String type, String value, String method, String description, String control) {
        this.id = id;
        this.type = type;
        this.value = value;
        this.method = method;
        this.description = description;
        this.control = control;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getControl() {
        return control;
    }

    public void setControl(String control) {
        this.control = control;
    }

    @Override
    public String toString() {
        return "ShiroResource{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", value='" + value + '\'' +
                ", method='" + method + '\'' +
                ", description='" + description + '\'' +
                ", control='" + control + '\'' +
                '}';
    }
}
