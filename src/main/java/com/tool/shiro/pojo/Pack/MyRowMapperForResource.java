package com.tool.shiro.pojo.Pack;

import com.tool.shiro.pojo.ShiroResource;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 实现RowMapper接口，返回Permission对象
 */
public class MyRowMapperForResource implements RowMapper<ShiroResource> {

    @Override
    public ShiroResource mapRow(ResultSet rs, int rowNum) throws SQLException {
        // 获取结果集数据
        Integer id = rs.getInt("id");
        String type = rs.getString("type");
        String value = rs.getString("value");
        String method = rs.getString("method");
        String description = rs.getString("description");
        String control = rs.getString("control");
         // 把数据封装成Permission对象
        ShiroResource shiroResource = new ShiroResource();
        shiroResource.setId(id);
        shiroResource.setType(type);
        shiroResource.setValue(value);
        shiroResource.setMethod(method);
        shiroResource.setDescription(description);
        shiroResource.setControl(control);
        return shiroResource;
    }

}
