package com.tool.shiro.pojo;

import java.io.Serializable;
import java.util.List;

/**
 * shiro工具用户实体类
 */
public class ShiroUser implements Serializable {

    // 序列化ID
    public static final long serialVersionUID = 1L;

    // 用户id
    private Integer id;
    // 用户账号
    private String account;
    // 用户昵称
    private String nickname;
    // 用户密码
    private String password;
    // 用户盐值
    private String salt;

    // 用户角色[一个用户可能有多个角色]
    private List<ShiroRole> shiroRoles;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public ShiroUser() {
    }

    public ShiroUser(Integer id, String account, String nickname, String password, String salt) {
        this.id = id;
        this.account = account;
        this.nickname = nickname;
        this.password = password;
        this.salt = salt;
    }

    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public List<ShiroRole> getRoles() {
        return shiroRoles;
    }

    public void setRoles(List<ShiroRole> shiroRoles) {
        this.shiroRoles = shiroRoles;
    }

    @Override
    public String toString() {
        return "ShiroUser{" +
                "id=" + id +
                ", account='" + account + '\'' +
                ", nickname='" + nickname + '\'' +
                ", password='" + password + '\'' +
                ", salt='" + salt + '\'' +
                ", shiroRoles=" + shiroRoles +
                '}';
    }

}
