package com.tool.shiro.pojo;

import org.apache.ibatis.type.Alias;

import java.io.Serializable;

/**
 * shiro工具权限实体类
 */

// 设置mybatis类型别名
@Alias(value = "ShiroPermission")
public class ShiroPermission implements Serializable {

    // 序列化ID
    public static final long serialVersionUID = 1L;

    // 权限id
    private Integer id;
    // 权限值 （user:*:* / *:*:* / admin:query:* / ……）
    private String value;
    // 描述
    private String description;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public ShiroPermission() {
    }

    public ShiroPermission(Integer id, String value, String description) {
        this.id = id;
        this.value = value;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "ShiroPermission{" +
                "id=" + id +
                ", value='" + value + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
