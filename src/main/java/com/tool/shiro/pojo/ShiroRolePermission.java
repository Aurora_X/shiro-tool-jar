package com.tool.shiro.pojo;

import java.io.Serializable;

/**
 * shiro工具角色-权限实体类
 */
public class ShiroRolePermission implements Serializable {

    // 序列化ID
    public static final long serialVersionUID = 1L;

    // 角色-权限表id
    private Integer id;
    // 角色id
    private Integer roleId;
    // 权限id
    private Integer permissionId;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public ShiroRolePermission() {
    }

    public ShiroRolePermission(Integer id, Integer roleId, Integer permissionId) {
        this.id = id;
        this.roleId = roleId;
        this.permissionId = permissionId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Integer permissionId) {
        this.permissionId = permissionId;
    }

    @Override
    public String toString() {
        return "ShiroRolePermission{" +
                "id=" + id +
                ", roleId=" + roleId +
                ", permissionId=" + permissionId +
                '}';
    }
}
