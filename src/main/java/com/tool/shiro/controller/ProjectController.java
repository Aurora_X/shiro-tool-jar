package com.tool.shiro.controller;

import com.tool.shiro.annotation.ApiInfoAnnotation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 所依赖项目的控制层接口
 */
@RestController
@RequestMapping(value = "/user")
public class ProjectController {

    @ApiInfoAnnotation(description = "用户测试接口_1")
    @RequiresRoles(value = {"admin", "user"}, logical = Logical.OR)           // 判断访问者是否属于该角色
    @RequiresPermissions(value = {"admin:*:*"}, logical = Logical.OR)            // 判断访问者是否具有该权限
    @RequestMapping("/test_1")
    public String test_1() {
        return "测试页_1";
    }

}
