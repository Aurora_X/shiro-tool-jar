package com.tool.shiro.controller;

import com.tool.shiro.annotation.ApiInfoAnnotation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 页面跳转Controller
 */
@Controller
@RequestMapping
public class PageController {

    /**
     * shiro工具包配置主页地址(可以直接在yml文件中配置)
     * shiro中设置该页面需认证授权后可访问
     * 未认证[登录]时跳转到 login 页面
     * 认证[登陆]后可跳转到 configIndex 配置主页
      */
    @RequestMapping(value = "${shiro.index}")
    @ApiInfoAnnotation(description = "跳转到shiro工具配置主页")
    //@RequiresPermissions("admin:*:*")
    public String index(){
        System.out.println("跳转到配置主页");
        return "configIndex";
    }

    /**
     * 跳转到login.html 登录页面
     */
    @RequestMapping(value = "/loginPage")
    @ApiInfoAnnotation(description = "跳转到登录页")
    public String loginPage() {
        System.out.println("跳转到登录页");
        return "login";
    }

    /**
     * 跳转到 permission.html 权限资源管理页面
     */
    @RequestMapping(value = "/permission", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiInfoAnnotation(description = "跳转到权限页")
    public String permission(){
        System.out.println("跳转到资源页");
        return "permission";
    }

    /**
     * 测试页
     */
    @RequestMapping(value = "/test", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiInfoAnnotation(description = "跳转到测试页")
    public String test(){
        System.out.println("跳转到测试页");
        return "test";
    }

}
