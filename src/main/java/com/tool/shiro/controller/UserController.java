package com.tool.shiro.controller;

import com.tool.shiro.annotation.ApiInfoAnnotation;
import com.tool.shiro.pojo.ShiroUser;
import com.tool.shiro.pojo.ShiroUserRole;
import com.tool.shiro.service.ShiroUserRoleService;
import com.tool.shiro.service.ShiroUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户表控制类
 */
@RestController
@RequestMapping(value = "/userManage")
public class UserController {

	@Autowired
	private ShiroUserService shiroUserService;

	@Autowired
	private ShiroUserRoleService shiroUserRoleService;

	@ApiInfoAnnotation(description = "添加shiro_user表用户")
	@RequestMapping(value = "/insertUser", method = RequestMethod.POST)
	public boolean insertUser(ShiroUser shiroUser) {
		return shiroUserService.insertUser(shiroUser);
	}

	@ApiInfoAnnotation(description = "添加用户-角色")
	@RequestMapping(value = "/insertUserRole", method = RequestMethod.POST)
	public boolean insertUserRole(ShiroUserRole shiroUserRole) {
		return shiroUserRoleService.insertUserRole(shiroUserRole);
	}

	@ApiInfoAnnotation(description = "更新 指定ID 用户角色")
	@RequestMapping(value = "/updateUserRole", method = RequestMethod.POST)
	public boolean updateUserRole(ShiroUserRole shiroUserRole) {
		return shiroUserRoleService.updateUserRoleById(shiroUserRole);
	}

	@ApiInfoAnnotation(description = "更新shiro_user用户")
	@RequestMapping(value ="/updateShiroUser", method = RequestMethod.POST)
	public boolean updateUser(ShiroUser shiroUser) {
		return shiroUserService.updateShiroUser(shiroUser);
	}

	@ApiInfoAnnotation(description = "删除shiro_user用户")
	@RequestMapping(value ="/deleteShiroUser", method = RequestMethod.POST)
	public boolean deleteUser(int id) {
		ShiroUser shiroUser = new ShiroUser();
		shiroUser.setId(id);
		return shiroUserService.deleteShiroUser(shiroUser);
	}

	@ApiInfoAnnotation(description = "删除 指定ID 用户-角色")
	@RequestMapping(value = "/deleteShiroUserRole", method = RequestMethod.POST)
	public boolean deleteUserRole(int id) {
		ShiroUserRole shiroUserRole = new ShiroUserRole();
		shiroUserRole.setId(id);
		return shiroUserRoleService.deleteUserRole(shiroUserRole);
	}


}
