package com.tool.shiro.controller;

import com.tool.shiro.annotation.ApiInfoAnnotation;
import com.tool.shiro.pojo.*;
import com.tool.shiro.service.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 角色、权限、资源表控制类
 * 不包含用户控制，因用户具有自定义多样化，放在其他Controller中
 */
@Controller
@RestController
@RequestMapping(value = "/manage")
public class RolePermissionResourceController {
	@Resource
	private ShiroResourceService shiroResourceService;

	@Resource
	private ShiroUserRoleService shiroUserRoleService;

	@Resource
	private ShiroRoleService shiroRoleService;

	@Resource
	private ShiroRolePermissionService shiroRolePermissionService;

	@Resource
	private ShiroPermissionService shiroPermissionService;

	public void test() {
		ShiroRole shiroRole = new ShiroRole(null, "测试", null);
		// 查询所有角色
		shiroRoleService.queryAllRoles();
		// 添加角色
		shiroRoleService.insertShiroRole(shiroRole);
		// 更新角色
		shiroRoleService.updateShiroRole(shiroRole);
		// 删除角色
		shiroRoleService.deleteShiroRole(shiroRole);

		// 查询所有权限
		shiroPermissionService.queryAllPermissions();
		// 查询指定角色所有权限
		shiroPermissionService.queryAllPermissionsByRole(shiroRole);

	}

	@ApiInfoAnnotation(description = "添加资源")
	@RequestMapping(value = "/insertShiroResource", method = RequestMethod.POST)
	public boolean insertResource(ShiroResource shiroResource) {
		return shiroResourceService.insertResource(shiroResource);
	}

	@ApiInfoAnnotation(description = "添加角色")
	@RequestMapping(value = "/insertShiroRole", method = RequestMethod.POST)
	public boolean insertRole(ShiroRole shiroRole) {
	return shiroRoleService.insertShiroRole(shiroRole);
}

	@ApiInfoAnnotation(description = "添加角色-权限")
	@RequestMapping(value = "/insertShiroRolePermission", method = RequestMethod.POST)
	public boolean insertRolePermission(ShiroRolePermission shiroRolePermission) {
		return shiroRolePermissionService.insertRolePermission(shiroRolePermission);
	}

	@ApiInfoAnnotation(description = "添加权限")
	@RequestMapping(value = "/insertShiroPermission", method = RequestMethod.POST)
	public boolean insertPermission(ShiroPermission shiroPermission) {
		return shiroPermissionService.insertPermission(shiroPermission);
	}

	@ApiInfoAnnotation(description = "删除 指定ID 资源")
	@RequestMapping(value ="/deleteShiroResource", method = RequestMethod.POST)
	public boolean deleteResource(int id) {
		return shiroResourceService.deleteShiroResource(id);
	}

	@ApiInfoAnnotation(description ="删除 指定ID 用户-角色")
	@RequestMapping(value ="/deleteShiroUserRoleById", method = RequestMethod.POST)
	public boolean deleteUserRoleById(int id) {
		ShiroUserRole shiroUserRole = new ShiroUserRole();
		shiroUserRole.setId(id);
		return shiroUserRoleService.deleteUserRole(shiroUserRole);
	}

	@ApiInfoAnnotation(description = "删除 指定ID 角色")
	@RequestMapping(value = "/deleteShiroRole", method = RequestMethod.POST)
	public boolean deleteRole(int id) {
		ShiroRole shiroRole = new ShiroRole(id, null, null);
		return shiroRoleService.deleteShiroRole(shiroRole);
	}

	@ApiInfoAnnotation(description = "删除 指定ID 角色-权限")
	@RequestMapping(value = "/deleteShiroRolePermission", method = RequestMethod.POST)
	public boolean deleteRolePermission(int id) {
		ShiroRolePermission shiroRolePermission = new ShiroRolePermission();
		shiroRolePermission.setId(id);
		return shiroRolePermissionService.deleteRolePermission(shiroRolePermission);
	}

	@ApiInfoAnnotation(description = "删除 指定ID 权限")
	@RequestMapping(value = "/deleteShiroPermission", method = RequestMethod.POST)
	public boolean deletePermission(ShiroPermission shiroPermission) {
		return shiroPermissionService.deletePermission(shiroPermission);
	}

	@ApiInfoAnnotation(description = "更新 指定ID 用户-角色")
	@RequestMapping(value = "/updateShiroUserRole", method = RequestMethod.POST)
	public boolean updateUserRole(ShiroUserRole shiroUserRole) {
		return shiroUserRoleService.updateUserRoleById(shiroUserRole);
	}

	@ApiInfoAnnotation(description = "更新 指定ID 角色")
	@RequestMapping(value = "/updateShiroRole", method = RequestMethod.POST)
	public boolean updateRole(ShiroRole shiroRole) {
		return shiroRoleService.updateShiroRole(shiroRole);
	}

	@ApiInfoAnnotation(description = "更新 指定ID 角色-权限")
	@RequestMapping(value = "/updateShiroRolePermission", method = RequestMethod.POST)
	public boolean updateRolePermission(ShiroRolePermission shiroRolePermission) {
		return shiroRolePermissionService.updateRolePermissionById(shiroRolePermission);
	}

	@ApiInfoAnnotation(description = "更新 指定ID 权限")
	@RequestMapping(value = "/updateShiroPermission", method = RequestMethod.POST)
	public boolean updatePermission(ShiroPermission shiroPermission) {
		return shiroPermissionService.updatePermissionById(shiroPermission);
	}

}
