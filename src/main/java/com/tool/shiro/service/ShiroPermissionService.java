package com.tool.shiro.service;

import com.tool.shiro.pojo.ShiroPermission;
import com.tool.shiro.pojo.ShiroRole;

import java.util.List;

/**
 * shiro_permission 权限service层接口
 */
public interface ShiroPermissionService {

	// 添加权限
	boolean insertPermission(ShiroPermission shiroPermission);

	// 查询所有权限
	List<ShiroPermission> queryAllPermissions();

	// 查询指定角色所有权限
	List<ShiroPermission> queryAllPermissionsByRole(ShiroRole role);

	// 更细 指定ID 权限
	boolean updatePermissionById(ShiroPermission shiroPermission);

	// 删除 指定ID 权限
	boolean deletePermission(ShiroPermission shiroPermission);

}
