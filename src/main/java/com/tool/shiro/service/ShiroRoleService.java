package com.tool.shiro.service;

import com.tool.shiro.pojo.ShiroRole;
import com.tool.shiro.pojo.ShiroUser;

import java.util.List;

/**
 * shiro_role 角色service层接口
 */
public interface ShiroRoleService {

	// 查询所有角色
	List<ShiroRole> queryAllRoles();

	// 添加角色
	boolean insertShiroRole(ShiroRole role);

	// 删除角色
	boolean deleteShiroRole(ShiroRole role);

	// 更新角色
	boolean updateShiroRole(ShiroRole role);

}
