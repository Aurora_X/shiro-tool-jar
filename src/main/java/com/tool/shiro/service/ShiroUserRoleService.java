package com.tool.shiro.service;

import com.tool.shiro.pojo.ShiroUserRole;

public interface ShiroUserRoleService {

	// 添加用户-角色
	boolean insertUserRole(ShiroUserRole shiroUserRole);

	// 更新 指定ID 用户-角色
	boolean updateUserRoleById(ShiroUserRole shiroUserRole);

	// 删除指定用户-角色
	boolean deleteUserRole(ShiroUserRole shiroUserRole);

}
