package com.tool.shiro.service;

import com.tool.shiro.pojo.ShiroResource;

import java.util.List;

/**
 * shiro_resource 资源Service接口
 */
public interface ShiroResourceService {

    // 添加资源
    boolean insertResource(ShiroResource shiroResource);

    // 获取所有权限信息
    List<ShiroResource> getAllShiroResourceInfo();

    // 获取所有指定类型权限信息
    List<ShiroResource> getShiroResourceInfoByType(String type);

    // 初始化权限表 [添加所有项目重启后的权限信息]
    int InitializationShiroResource(List<ShiroResource> shiroResources);

    // 删除指定类型权限
    int deleteShiroResourcesByType(String type);

    // 删除指定资源
    boolean deleteShiroResource(int id);

}
