package com.tool.shiro.service;

import com.tool.shiro.pojo.*;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * Shiro管理员Service接口
 */
public interface ShiroAdminService {

    // 通过account管理员账号查询管理员信息
    ShiroAdminManage findByAccount(String account);

    // 查询账号是否存在(true: 存在    false: 不存在)
    boolean findAccountIsExist(String account);

    // 注册(新增/添加)管理员(true: 注册成功      false: 注册失败)
    boolean insertShiroAdmin(ShiroAdminManage shiroAdmin);

    // 获取数据库相关信息(库及表信息)
    Map<String, Object> getDataBaseInfo() throws SQLException;

    // 后端获取接口信息修改数据库资源表
    boolean updateShiroResource() throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException;

    // 通过数据库权限信息重新设置权限
    boolean resetShiroResourceByDB();

    // 获取所有权限资源
    List<ShiroResource> getAllShiroResources();

    // 修改权限资源
    boolean updateShiroResource(ShiroResource shiroResource);

    // 查询表是否存在
    boolean existsTables(List<String> tableNames) throws SQLException;

    // 查询指定管理员角色列表
    List<ShiroRole> queryRolesByUser(ShiroAdminManage user);

    // 查询指定用户权限列表
    List<ShiroPermission> queryPermissionsByUser(ShiroAdminManage user);

    // 删除 指定ID shiro管理员
    boolean deleteShiroAdminById(ShiroAdminManage shiroAdminManage);

    // 更新 指定ID shiro管理员
    boolean updateShiroAdmin(ShiroAdminManage shiroAdminManage);

}
