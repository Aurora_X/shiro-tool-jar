package com.tool.shiro.service;

import com.tool.shiro.pojo.ShiroPermission;
import com.tool.shiro.pojo.ShiroRole;
import com.tool.shiro.pojo.ShiroUser;

import java.util.List;

/**
 * shiro_user 用户service层接口
 */
public interface ShiroUserService {

	// 添加shiro_user用户
	boolean insertUser(ShiroUser shiroUser);

	// 查询 指定用户ID 角色列表
	List<ShiroRole> queryRolesByUser(ShiroUser user);

	// 查询 指定account 用户信息
	ShiroUser queryUser(ShiroUser user);

	// 查询 指定用户ID 权限列表
	List<ShiroPermission> queryPermissionsByUser(ShiroUser user);

	// 修改指定ShiroUser用户
	boolean updateShiroUser(ShiroUser user);

	// 删除指定ID shiro_user 表用户
	boolean deleteShiroUser(ShiroUser shiroUser);

}
