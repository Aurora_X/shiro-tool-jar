package com.tool.shiro.service;

import com.tool.shiro.pojo.ShiroPermission;

import java.util.List;

public interface RoleService {

    // 通过roleId角色Id查询对应权限
    List<ShiroPermission> findPermissionByRoleId(Integer roleId);

}
