package com.tool.shiro.service.impl;

import com.tool.shiro.mapper.basic.ShiroRolePermissionMapper;
import com.tool.shiro.pojo.ShiroRolePermission;
import com.tool.shiro.service.ShiroRolePermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class ShiroRolePermissionServiceImpl implements ShiroRolePermissionService {

	@Resource
	private ShiroRolePermissionMapper shiroRolePermissionMapper;

	@Override
	public boolean insertRolePermission(ShiroRolePermission shiroRolePermission) {
		return shiroRolePermissionMapper.insertRolePermission(shiroRolePermission) == 1;
	}

	@Override
	public boolean updateRolePermissionById(ShiroRolePermission shiroRolePermission) {
		return shiroRolePermissionMapper.updateRolePermission(shiroRolePermission) == 1;
	}

	@Override
	public boolean deleteRolePermission(ShiroRolePermission shiroRolePermission) {
		return shiroRolePermissionMapper.deleteRolePermissionById(shiroRolePermission) == 1;
	}

}
