package com.tool.shiro.service.impl;

import com.tool.shiro.mapper.basic.ShiroPermissionMapper;
import com.tool.shiro.mapper.basic.ShiroRolePermissionMapper;
import com.tool.shiro.pojo.ShiroPermission;
import com.tool.shiro.pojo.ShiroRole;
import com.tool.shiro.service.ShiroPermissionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ShiroPermissionServiceImpl implements ShiroPermissionService {

	@Resource
	private ShiroPermissionMapper shiroPermissionMapper;

	@Resource
	private ShiroRolePermissionMapper shiroRolePermissionMapper;

	@Override
	public boolean insertPermission(ShiroPermission shiroPermission) {
		return shiroPermissionMapper.insertPermission(shiroPermission) == 1;
	}

	@Override
	public List<ShiroPermission> queryAllPermissions() {
		return shiroPermissionMapper.queryAllPermissions();
	}

	@Override
	public List<ShiroPermission> queryAllPermissionsByRole(ShiroRole role) {
		// 获取所有 指定角色ID 的权限ID
		List<Integer> idList = shiroRolePermissionMapper.queryRolePermissionsByRoleId(role);
		// 获取 指定ID 权限列表
		return shiroPermissionMapper.queryPermissionsByIds(idList);
	}

	@Override
	public boolean updatePermissionById(ShiroPermission shiroPermission) {
		return shiroPermissionMapper.updatePermission(shiroPermission) == 1;
	}

	@Override
	public boolean deletePermission(ShiroPermission shiroPermission) {
		return shiroPermissionMapper.deletePermission(shiroPermission) == 1;
	}

}
