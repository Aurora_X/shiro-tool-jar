package com.tool.shiro.service.impl;

import com.tool.shiro.mapper.basic.ShiroRoleMapper;
import com.tool.shiro.pojo.ShiroRole;
import com.tool.shiro.pojo.ShiroUser;
import com.tool.shiro.service.ShiroRoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ShiroRoleServiceImpl implements ShiroRoleService {

	@Resource
	private ShiroRoleMapper shiroRoleMapper;

	@Override
	public List<ShiroRole> queryAllRoles() {
		return null;
	}

	@Override
	public boolean insertShiroRole(ShiroRole role) {
		return shiroRoleMapper.insertRole(role) == 1;
	}

	@Override
	public boolean deleteShiroRole(ShiroRole role) {
		return shiroRoleMapper.deleteRole(role) == 1;
	}

	@Override
	public boolean updateShiroRole(ShiroRole role) {
		return shiroRoleMapper.updateRole(role) == 1;
	}
}
