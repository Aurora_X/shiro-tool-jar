package com.tool.shiro.service.impl;

import com.tool.shiro.dao.ShiroResourceDao;
import com.tool.shiro.mapper.basic.ShiroResourceMapper;
import com.tool.shiro.pojo.ShiroResource;
import com.tool.shiro.service.ShiroResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class ShiroResourceServiceImpl implements ShiroResourceService {

    @Autowired
    private ShiroResourceDao shiroResourceDao;

    @Resource
    private ShiroResourceMapper shiroResourceMapper;

    @Override
    public boolean insertResource(ShiroResource shiroResource) {
        return shiroResourceMapper.insertResource(shiroResource) == 1;
    }

    @Override
    public List<ShiroResource> getAllShiroResourceInfo() {
        return shiroResourceDao.getAllShiroResourceInfo();
    }

    @Override
    public List<ShiroResource> getShiroResourceInfoByType(String type) {
        return shiroResourceDao.getShiroResourceInfoByType(type);
    }

    @Override
    public int InitializationShiroResource(List<ShiroResource> shiroResources) {
        return shiroResourceDao.InitializationShiroResource(shiroResources);
    }

    @Override
    public int deleteShiroResourcesByType(String type) {
        return shiroResourceDao.deleteShiroResourcesByType(type);
    }

    @Override
    public boolean deleteShiroResource(int id) {
        ShiroResource shiroResource = new ShiroResource();
        shiroResource.setId(id);
        return shiroResourceMapper.deleteResource(shiroResource) == 1;
    }

}
