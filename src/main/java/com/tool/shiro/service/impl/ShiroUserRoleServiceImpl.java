package com.tool.shiro.service.impl;

import com.tool.shiro.mapper.basic.ShiroUserRoleMapper;
import com.tool.shiro.pojo.ShiroUserRole;
import com.tool.shiro.service.ShiroUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class ShiroUserRoleServiceImpl implements ShiroUserRoleService {

	@Resource
	private ShiroUserRoleMapper shiroUserRoleMapper;

	@Override
	public boolean insertUserRole(ShiroUserRole shiroUserRole) {
		return shiroUserRoleMapper.insertUserRole(shiroUserRole) == 1;
	}

	@Override
	public boolean updateUserRoleById(ShiroUserRole shiroUserRole) {
		return shiroUserRoleMapper.updateUserRole(shiroUserRole) == 1;
	}

	@Override
	public boolean deleteUserRole(ShiroUserRole shiroUserRole) {
		return shiroUserRoleMapper.deleteUserRoleById(shiroUserRole) == 1;
	}

}
