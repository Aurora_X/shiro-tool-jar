package com.tool.shiro.service;

import com.tool.shiro.pojo.ShiroRolePermission;

public interface ShiroRolePermissionService {

	// 添加角色权限
	boolean insertRolePermission(ShiroRolePermission shiroRolePermission);

	// 更新 指定ID 角色-权限
	boolean updateRolePermissionById(ShiroRolePermission shiroRolePermission);

	// 删除 指定ID 角色-权限
	boolean deleteRolePermission(ShiroRolePermission shiroRolePermission);

}
