package com.tool.shiro.dao;

import com.tool.shiro.pojo.ShiroAdminManage;

import java.sql.SQLException;
import java.util.Map;

/**
 * Dao层实现基本的数据库增删改查,不做数据的校验等
 */
public interface ShiroAdminDao {

    // 通过account管理员账号查询管理员信息
    ShiroAdminManage findByAccount(String account);

    // 查询账号是否存在(true: 存在    false: 不存在)
    boolean findAccountIsExist(String account);

    // 注册(新增/添加)管理员
    int insertShiroAdmin(ShiroAdminManage shiroAdmin);

    // 获取数据库相关信息(库及表信息)
    Map<String, Object> getDataBaseInfo() throws SQLException;
}
