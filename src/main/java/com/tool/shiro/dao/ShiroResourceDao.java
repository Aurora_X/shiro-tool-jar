package com.tool.shiro.dao;

import com.tool.shiro.pojo.ShiroResource;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ShiroResourceDao {

    // 获取所有权限信息
    List<ShiroResource> getAllShiroResourceInfo();

    // 获取所有指定类型权限信息
    List<ShiroResource> getShiroResourceInfoByType(String type);

    // 初始化权限表 [添加所有项目重启后的权限信息]
    int InitializationShiroResource(List<ShiroResource> shiroResources);

    // 删除指定类型权限
    int deleteShiroResourcesByType(String type);

}
