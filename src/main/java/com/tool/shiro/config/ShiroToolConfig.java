package com.tool.shiro.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * 注意：引入的项目中需要在某个地方加入该配置类进行扫描
 * 将下列所有注释放开即可
 */
//@Configuration
//@MapperScan({"com.tool.shiro.mapper", "com.tool.shiro.mapper.basic"})
//@ComponentScan(basePackages = {
//		"com.tool.shiro.config",
//		"com.tool.shiro.controller",
//		"com.tool.shiro.pojo",
//		"com.tool.shiro.service.impl",
//		"com.tool.shiro.service",
//		"com.tool.shiro.dao"
//	})
public class ShiroToolConfig {

}
