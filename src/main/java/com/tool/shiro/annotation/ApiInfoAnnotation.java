package com.tool.shiro.annotation;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
// 方法和类级别注解
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface ApiInfoAnnotation {

    // 接口描述(如接口用于某某功能)
    String description() default "";

}
