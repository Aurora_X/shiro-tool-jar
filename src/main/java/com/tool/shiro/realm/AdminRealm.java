package com.tool.shiro.realm;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

/**
 * 自定义项目管理员Realm
 * 项目需要自定义管理员realm时继承该类
 * 注意: service层等定制化的类和方法、常量等需要自己写好改好后放进来并进行修改
 * 完全继承则数据库管理员表名默认为 admin
 */
public class AdminRealm extends AuthorizingRealm {

	/**
	 * 授权信息
	 * @param principalCollection       用户凭证信息
	 * @return      授权信息
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
		return null;
	}

	/**
	 * 认证信息
	 * @param authenticationToken       认证token信息
	 * @return      认证信息
	 * @throws AuthenticationException  抛出认证错误
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
		return null;
	}

}
